﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace treci
{
    class Program
    {
        static void Main(string[] args)
        {   
            //3
            List<IRentable> list = new List<IRentable>();
            Video movie = new Video("Mr & Mrs Smith");
            Book book = new Book("Divergent");
            list.Add(movie);
            list.Add(book);
            RentingConsolePrinter rent = new RentingConsolePrinter();
            rent.DisplayItems(list);
            rent.PrintTotalPrice(list);

            //4
            List<IRentable> list2 = new List<IRentable>();
            HotItem book1 = new HotItem(new Book("Divergent 2"));
            list2.Add(book1);
            HotItem movie1 = new HotItem(new Video("Mr & Mrs Smith 2"));
            list2.Add(movie1);
            RentingConsolePrinter rent2 = new RentingConsolePrinter();
            rent2.DisplayItems(list2);
            rent2.PrintTotalPrice(list2);


            //5
            List<IRentable> flashsale = new List<IRentable>();
            DiscountedItem num1 = new DiscountedItem(movie);
            DiscountedItem num2 = new DiscountedItem(book);
            flashsale.Add(num1);
            flashsale.Add(num2);
            RentingConsolePrinter rent3 = new RentingConsolePrinter();
            rent3.DisplayItems(flashsale);
            rent3.PrintTotalPrice(flashsale);

        }
    }
}
