﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prvi
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;
            Dataset data = new Dataset("program.txt");
            Analyzer3rdParty analyzer = new Analyzer3rdParty();
            Adapter adapter = new Adapter(analyzer);
            

            double[] aritcol = adapter.CalculateAveragePerColumn(data);
            double[] aritrow = adapter.CalculateAveragePerRow(data);

           

            foreach (double num in aritrow)
            {
                Console.WriteLine(num);
            }

            Console.WriteLine(" ");

            foreach ( double number in aritcol)
            {
                Console.WriteLine(number);
            }

            
        }
    }
}
